import range from './range';
const yearSelect = document.querySelector('.year-select');
const label = yearSelect.querySelector('.form-label');

yearSelect.onclick = () => {
  if (event.target.classList.contains('year-select')) {
    yearSelect.classList.add('open')
  }

  if (event.target.tagName === 'A') {
    yearSelect.classList.remove('open');
    label.classList.add('fill')
    label.innerText = event.target.text
  }
}

document.onclick = (e) => {
  
  if (!e.target.classList.contains('open') && !e.target.classList.contains('year-select-list'))
    yearSelect.classList.remove('open');
}

const menuBtn = document.querySelector('.open-menu');
menuBtn.onclick = function () {
  document.body.classList.toggle('open');
  if (document.body.classList.contains('open')) {
    document.body.style.overflow = 'hidden';
  } else {
    document.body.style.overflow = 'auto';
  }
}

const menuHrefs = document.querySelectorAll('header nav a');
for (let index = 0; index < menuHrefs.length; index++) {
  const element = menuHrefs[index];
  element.onclick = () => {
    document.body.classList.remove('open');
    document.body.style.overflow = 'auto';
  }

}


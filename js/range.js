

const progressBar = document.querySelector('.range .progress-bar'),
  range = document.querySelector('.range'),
  rangeValue = document.querySelector('.range .progress-bar .progress-bar-value'),
  nativeRange = document.getElementById('nativeRange');

function setChange() {
  var evt = document.createEvent("HTMLEvents");
  evt.initEvent("change", false, true);
  nativeRange.dispatchEvent(evt);
}

function setRangeValue(x) {
  const pos = range.getBoundingClientRect();
  let perc = ((parseInt(x) - parseInt(pos.left))) / parseInt(pos.width) * 100;
  perc = perc < 0 ? perc : perc;
  nativeRange.value = perc;
  setChange();
}

progressBar.onclick = (e) => {
  setRangeValue(e.pageX);
}

let mouseDown = false;

rangeValue.addEventListener('mousedown', (e) => {
  e.preventDefault();
  mouseDown = true;
});

document.addEventListener('mouseup', () => {
  mouseDown = false;
});

document.addEventListener('mousemove', (e) => {
  if (mouseDown) {
    setRangeValue(e.pageX)
  }
});

rangeValue.addEventListener('touchstart', (e) => {
  e.preventDefault();
  mouseDown = true;
});

document.addEventListener('touchend', () => {
  mouseDown = false;
});

document.addEventListener('touchmove', (e) => {
  if (mouseDown) {
    setRangeValue(e.touches[0].clientX)
  }
});



function showVisibleTicks() {
  const ticks = document.querySelectorAll('.tick-container .tick');
  if (window.innerWidth > 700) {
    for (let index = 0; index < ticks.length; index++) {
      const el = ticks[index];
      el.style.visibility = 'visible';
    }
    return;
  }

  const pos = range.getBoundingClientRect();
  let elVisible = [];
  for (let index = 0; index < ticks.length; index++) {
    const el = ticks[index];
    const elPos = el.getBoundingClientRect();
    const rangePosPx = (parseInt(nativeRange.value) / 100) * pos.width;
    el.style.visibility = 'hidden';

    if (rangePosPx > elPos.left && rangePosPx < (elPos.left + elPos.width) - pos.left)
      elVisible.push(el)
  }

  const el = elVisible[elVisible.length - 1];
  if (el) {
    el.style.visibility = 'visible';
  }
}

nativeRange.addEventListener('change', (e) => {
  rangeValue.style.width = nativeRange.value + '%';
  showVisibleTicks();
});

setChange();
window.onresize = () => {
  setChange();
}
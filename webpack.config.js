const path = require('path');

module.exports = {
  entry: './js/script.js',
  //devtool: 'inline-source-map',
  watchOptions: {
    aggregateTimeout: 300,
    poll: 1000
  },
  module: {
    rules: [
      {
         use: {
            loader:'babel-loader',
            options: { presets: ['@babel/preset-env'] }
         },
         test: /\.js$/
      }
    ]
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js']
  },
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, './build')
  }
};